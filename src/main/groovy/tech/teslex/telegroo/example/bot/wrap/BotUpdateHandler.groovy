package tech.teslex.telegroo.example.bot.wrap

import groovy.transform.CompileStatic
import tech.teslex.telegroo.simple.update.SimpleUpdateHandler

@CompileStatic
interface BotUpdateHandler extends SimpleUpdateHandler {}