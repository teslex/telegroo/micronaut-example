package tech.teslex.telegroo.example.bot.commands

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import tech.teslex.telegroo.example.annotation.Component
import tech.teslex.telegroo.simple.context.SimpleCommandContext
import tech.teslex.telegroo.simple.update.SimpleCommandPatternUpdateHandler

import javax.inject.Singleton
import java.util.regex.Pattern

@Component
class StartCommand implements SimpleCommandPatternUpdateHandler {

	Pattern pattern = ~/start/

	Pattern argsPattern

	@Override
	void handle(SimpleCommandContext context) {
		context.sendMessage(text: 'Welcome!')
	}
}
