package tech.teslex.telegroo.example.init

import io.micronaut.context.event.ApplicationEventListener
import io.micronaut.context.event.StartupEvent
import tech.teslex.telegroo.example.annotation.Component
import tech.teslex.telegroo.example.bot.MicronautTelegroo
import tech.teslex.telegroo.example.bot.wrap.BotUpdateHandler
import tech.teslex.telegroo.example.conf.BotConfig
import tech.teslex.telegroo.simple.update.SimpleCallbackQueryUpdateHandler
import tech.teslex.telegroo.simple.update.SimpleCommandPatternUpdateHandler
import tech.teslex.telegroo.simple.update.SimpleMessagePatternUpdateHandler

import javax.inject.Inject

@Component
class OnStartup implements ApplicationEventListener<StartupEvent> {

	@Inject
	BotConfig config

	@Inject
	MicronautTelegroo telegroo

	@Inject
	List<SimpleCommandPatternUpdateHandler> commandsHandlers

	@Inject
	List<SimpleMessagePatternUpdateHandler> messagesHandlers

	@Inject
	List<SimpleCallbackQueryUpdateHandler> callbackQueryHandlers

	@Inject
	List<BotUpdateHandler> updateHandlers

	@Override
	void onApplicationEvent(StartupEvent event) {
		updateHandlers.each(telegroo.&updateHandler)
		messagesHandlers.each(telegroo.&messageUpdateHandler)
		commandsHandlers.each(telegroo.&commandUpdateHandler)
		callbackQueryHandlers.each(telegroo.&callbackQueryUpdateHandler)

		log.info("Token: $telegroo.token")
		log.warn(telegroo.mainContext.setWebhook(url: config.webhookUrl) as String)
	}
}
